# Exemplo C# de cadastro de coleta

Um exemplo de cadastro de coleta via Web Service utilizando a lingagem C# com o pacote [RestSharp](http://restsharp.org/).

## Requisitos

- Docker

## Imagem docker

`docker image build exemplo-cadastro-coleta-csharp .`

## Executar container

`docker container run -e "SCAD_URL=[SCAD_URL]" -e "AUTH_TOKEN=[AUTH_TOKEN]" -e "PARTICIPANTE_CPF=[PARTICIPANTE_CPF]" -e "PARTICIPANTE_EMAIL=[PARTICIPANTE_EMAIL]" exemplo-cadastro-coleta-csharp`

Substituir os valores com a expressão `[XXX]` conforme:

**Variaveis**:

| Nome               | Descriao                                          | Exemplo                  |
| ------------------ | ------------------------------------------------- | ------------------------ |
| SCAD_URL           | A base da URL do web service                      | https://cloud.bry.com.br |
| AUTH_TOKEN         | O token de autenticação para o cadastro da coleta |                          |
| PARTICIPANTE_CPF   | O CPF do assinante da coleta                      | 00000000000              |
| PARTICIPANTE_EMAIL | O E-mail do assinante da coleta                   | email@email.com          |
