﻿using System;
using System.IO;
using System.Text;
using RestSharp;

namespace app
{
    public class Program
    {

        public static void Main()
        {
            var client = new RestClient(string.Format("{0}/scad/rest/coletas/cadastrar", Environment.GetEnvironmentVariable("SCAD_URL")));
            var request = new RestRequest(Method.POST);
            var now = DateTime.Now;
            var nomeColeta = string.Format("Teste cadastro C# {0}", now.ToString("yyyy MM dd HHmmss"));
            var dataLimite = Program.FormatDate(now.AddDays(7));

            Console.WriteLine(string.Format("Utilizando data limite: {0}", dataLimite));

            request.AddHeader("t", Environment.GetEnvironmentVariable("AUTH_TOKEN"));
            request.AddQueryParameter("nomeColeta", nomeColeta);
            request.AddQueryParameter("exigirDownload", "false");
            request.AddQueryParameter("proibirRejeicao", "false");
            request.AddQueryParameter("assinaturaSequencial", "false");
            request.AddQueryParameter("agruparDocumentos", "true");
            request.AddQueryParameter("configuracaoLocalAssinatura", "true");
            request.AddQueryParameter("padraoAssinatura", "PDF");
            request.AddQueryParameter("dataLimite", dataLimite);

            request.AddHeader("Content-Type", "multipart/form-data");

            request.AddFile("documento", File.ReadAllBytes("/app/demo.docx"), "teste.docx", "application/octet-stream");
            request.AddFile("documento", File.ReadAllBytes("/app/demo.docx"), "teste2.docx", "application/octet-stream");
            
            var p = new Participante();
            p.nome = "Assinante teste";
            p.email = Environment.GetEnvironmentVariable("PARTICIPANTE_EMAIL");
            p.codigo = Environment.GetEnvironmentVariable("PARTICIPANTE_CPF");
            p.assinante = true;
            
            var bytesParticipante = Encoding.UTF8.GetBytes(SimpleJson.SerializeObject(p));
            var nomeArquivoParticipante = string.Format("participante_{0}.json", p.codigo);
            request.AddFile("participante", bytesParticipante, nomeArquivoParticipante, "application/json");
            
            var response = client.Execute(request);
            
            Console.WriteLine(string.Format("Resposta da requisição: \n   Status: {0}\n   Content: {1}", response.StatusCode, response.Content));
            
        }

        public static string FormatDate(DateTime date) {
            return date.ToString("yyyyMMdd'T'HHmmssK").Replace(":", "");
        }

    }
    
    public class Participante 
    {
    
        public string nome { get; set; }
        
        public string codigo { get; set; }
        
        public string email { get; set; }
        
        public bool assinante { get; set; }
        
        public bool revisor { get; set; }
        
    }
}
